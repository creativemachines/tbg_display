#include <Arduino.h>
bool draw(int curx, int cury, int x0, int y0, int w, int h, bool bmp[]){
  if(curx>=x0 && curx<x0+w){
    if(cury>=y0 && cury<y0+h){
      int pxl = (curx-x0)+((cury-y0)*w);
      if(bmp[pxl]){return 1;}
    }
  }
  return 0;
}
