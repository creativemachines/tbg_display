/*  
 *   Required Connections
  --------------------
    pin 2:  LED Strip #1    OctoWS2811 drives 8 LED Strips.
    pin 14: LED strip #2    All 8 are the same length.
    pin 7:  LED strip #3
    pin 8:  LED strip #4    A 100 ohm resistor should used
    pin 6:  LED strip #5    between each Teensy pin and the
    pin 20: LED strip #6    wire to the LED strip, to minimize
    pin 21: LED strip #7    high frequency ringining & noise.
    pin 5:  LED strip #8
    pin 15 & 16 - Connect together, but do not use
    pin 4 - Do not use
    pin 3 - Do not use as PWM.  Normal use is ok.

  This test is useful for checking if your LED strips work, and which
  color config (WS2811_RGB, WS2811_GRB, etc) they require.
*/

#include "OctoWS2811.h"
#include "HSBColor.h"
#include "bitmap.h"

//Octo library config
const int ledsPerStrip = 16*32;
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
const int config = WS2811_GRB | WS2811_800kHz;
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

//485 Transmit interrupt
IntervalTimer timeout485;
unsigned long lastTx=0;      //Last time data was sent
unsigned long commsTimeout=30;  //Time to wait for reply

//IO pins
const byte OCTOEN = 33;
const byte DE1_485 = 28;
const byte RE1_485 = 29;
const byte DE2_485 = 30;
const byte RE2_485 = 31;

//Input header
const byte D1 = 12;
const byte D2 = 3;
const byte D3 = 4;

//DIP switches
const byte dip1 = 24;
const byte dip2 = 25;
const byte dip3 = 26;
const byte dip4 = 27;

//Pots
const byte pot1 = 23;
const byte pot2 = 15;
const byte pot3 = 22; //Brightness

// LEDs
const byte led = 13;
const byte auxled =30;
IntervalTimer heartbeat;
bool hb=0;

//State tracking
bool victor=0;
bool quit=0;
unsigned char myState=0;
unsigned long myTime=0;
unsigned long opponentTime=0;
unsigned long opponentTimeout=0;
unsigned long lastRx=0;
unsigned char opponentState=0;
unsigned char prevOpponentState=0;
const unsigned char IDLE=10;
const unsigned char ACTIVE=15;
const unsigned char FORFEIT=20;
const unsigned char FINISH=25;

//Variables
bool comsFailure=0;
bool raceLoop=1;
const int width = 16;
const int height = 160;
byte brightness=10;
byte minBrightness = 5;
byte maxBrightness = 35;
int bright;
int color;
int cycle;
int cycle_mod;
int solid;
int vin;
int watts;
int currentRow=0;
int graph[height];
float vavgFactor=50;
float vavg;
float vdisp;
float vscale = 0; //Percentage of max voltage
unsigned long raceBegin = 0;
unsigned long raceTimeout = 0;
unsigned long resetTime=0;
unsigned long timestep=0;
unsigned long total;
unsigned long finishlineMin = 30000; 
unsigned long finishlineMax = 300000; 
unsigned long finishline = 300000; 

//Time constants
const unsigned long buttonIgnoreTime=1000;
const unsigned long raceTimeoutTime=4000;
const unsigned long victoryDisplayTime=3000;
const unsigned long raceEndDisplayTime = 8000;
const unsigned long totalLimitTime = 120000;
const unsigned long comsTimeout = 1000000;

void setup() {
  //wrap the cycle at a multiple of 360 to avoid hiccup
  cycle_mod=((height*width)/360)*360;
  pinMode(OCTOEN, OUTPUT);
  pinMode(DE1_485, OUTPUT);
  pinMode(RE1_485, OUTPUT);
  pinMode(DE2_485, OUTPUT);
  pinMode(RE2_485, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(auxled, OUTPUT);
  pinMode(D1, INPUT);
  pinMode(dip1, INPUT_PULLUP);
  pinMode(dip2, INPUT_PULLUP);
  pinMode(dip3, INPUT_PULLUP);
  pinMode(dip4, INPUT_PULLUP);
  pinMode(pot1, INPUT);
  pinMode(pot2, INPUT);
  pinMode(pot3, INPUT);
  digitalWrite(OCTOEN,0);
  digitalWrite(DE1_485,1);
  digitalWrite(RE1_485,0);
  digitalWrite(DE2_485,0);
  digitalWrite(RE2_485,0);
  Serial.begin(115200);
  Serial1.begin(38400, SERIAL_8N1);
  Serial2.begin(1000000, SERIAL_8N1);
  leds.begin();
  leds.show();
  Serial1.print("\r<ID 0><CLR>\r");
}

void loop() {
  raceReset();
  Serial1.flush();
  Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0><SL><S S><BL N><CS 1><GRN><T>Press START button to begin</T>\r");
  //Read out any data in the buffer
  while(Serial2.available()){Serial2.read();}
  digitalWrite(led,1);
  //Begin idle loop
  while(1){
    updateOpponent;
    updateFinishline();
    brightness=analogRead(pot3)/1024.0*100;
    brightness=constrain(brightness, minBrightness, maxBrightness);
    digitalWrite(led, 1);
    //Check for incoming serial
    if(Serial2.available()>0){  
      //if data is "s" go into race
      if(Serial2.read()==115){
        Serial2.flush();  //clear buffer
        break;
      }
    }
    //Start button delay to handle people holding it down
    if(millis()-resetTime > buttonIgnoreTime){
      if(!digitalRead(D1)){
        send485(115);
        break;
      }
    }
    idleDisplay();
    if(!digitalRead(dip4)){ graphDisplay(float(finishline-finishlineMin)/(finishlineMax-finishlineMin));  }
    leds.show();
  }
  //Race lead-in
  heartbeat.begin(heartbeatISR, 200000);
  digitalWrite(led, 0);
  countdown();
  raceTimeout=millis();
  raceBegin=millis();
  total=0;
  clearGraph();
  //Race loop
  digitalWrite(led,0);
  while(raceLoop){
    race();
    //VictoryClock if opponentFinishes first
    if(opponentTime>10 && millis()-opponentTime < victoryDisplayTime){ 
        victoryClock(opponentTime); 
    }
    else{ updateClock(); }
    //Idle timeout flag
    if (vscale>0.1){  myState=ACTIVE; raceTimeout=millis(); }
    //Handle maximum timeout
    if (millis()-raceBegin > totalLimitTime){ break; }
    //Handle idle timeout
    if((millis()-raceTimeout)>raceTimeoutTime){
      myState=IDLE;
      if(total>100){ myState=FORFEIT; }
      //If idle and the other unit is not responding, reset
      if(comsFailure){
        Serial.println("Coms Timeout");
        break; 
      }
      if(opponentState==IDLE || opponentState==FORFEIT ){
        Serial.println("Opponent IDLE or FORFEIT");
        raceLoop=0; 
      } 
      if(opponentState==FINISH){ 
        Serial.println("Opponent FINISH, quit");
        quit=1;
      }
    }
    if(total>finishline){
      Serial.println("Finishline reached");
      myState=FINISH;
      send485(FINISH);   //Notify opponent of finish
    }
    updateOpponent();
    if(myState==FINISH || quit){
      victor=1;
      if(opponentState==FINISH || opponentState==IDLE){victor=0;}
      long victoryTimeout=millis();
      myTime=victoryTimeout;
  heartbeat.begin(heartbeatISR, 100000);
      while(1){
        checkeredFlag();
        if(victor){victory();}
        //Update buffer with checkeredFlag and victory, then led.show to prevent flicker
        leds.show();
        //Check serial
        updateOpponent();
        if(opponentState!=FINISH && ((millis()-myTime)<victoryDisplayTime)){
          victoryClock(myTime); 
        }
        else if(opponentTime>10 && millis()-opponentTime < victoryDisplayTime){ 
          victoryClock(opponentTime); 
        }
        else if(myState==FINISH && (opponentState==IDLE || opponentState == FORFEIT)){
          soloClock(myTime);
        }
        else if((myState==IDLE || myState == FORFEIT) && opponentState==FINISH){
          soloClock(opponentTime);
        }
        else if(myState==FINISH && opponentState==FINISH){
          finaleClock();
        }
        else{updateClock();}
        victoryTimeout=max(myTime, opponentTime);
        if(millis()-victoryTimeout > raceEndDisplayTime && (opponentState==IDLE || opponentState==FORFEIT || opponentState==FINISH )){
          break;
        }
      }
      break;
    }
  } 
  raceReset();
  heartbeat.end();
}

//Clear all race state variables
void raceReset(){
  victor=0;
  quit=0;
  myState=IDLE;
  myTime=0;
  opponentTime=0;
  opponentTimeout=0;
  opponentState=IDLE;
  prevOpponentState=0;
  lastRx=0;
  raceLoop=1;
  vavg=0;
  vscale=0;
  hb=0;
  resetTime=millis();
}

void race(){
  //vin = analogRead(pot1); //DEBUG ONLY
  vin = analogRead(A2); 
  total+= watts/(millis()-timestep);
  timestep=millis();
  vavg = ((vscale*vavgFactor)+(vin))/(vavgFactor+1);
  watts = vavg * vavg * 9.8;
  vscale = vavg/20;     //Convert voltage to float percentage of max
  cycle+= vscale*5;     //Scale color cycle by voltage
  cycle=cycle%cycle_mod;
  currentRow=min((total*1.0/finishline)*height, height-1);
  graph[currentRow]=vscale*width*10;
  for(byte row=0; row<height; row++){
    for(byte col=0; col<width; col++){      
        if(row<height-currentRow){color=0;}
        else{
          bright=max(51,graph[height-row]);
          if(col<8){ 
            bright=bright-((15-col)*6);
          }
          else{ 
            bright=bright-(col*6);
          }
          bright=constrain(bright,0,brightness);
          unsigned int wtf=row*4+(359-(cycle%360));
          wtf=wtf%360;
          color=H2R_HSBtoRGB(wtf,255,bright);
        }
        leds.setPixel(mapPixel((row*width)+col),color);
    }
  }
  leds.show();
}

void countdown(){
  //Count on ViewMarq
  Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0><SL><S S><BL N><CS 1><AMB><T>Start pedalling!</T>\r");
  cycle=0;
  int charBuffer=0;
  //Countdown bitmaps
  bool* countBuffer[]={ threebmp, twobmp, onebmp, gobmp };
  //dimensions of bitmaps
  int locations[][4]={{5,40,5,9},
                      {5,40,5,8},
                      {5,40,5,8},
                      {0,40,16,8}};
  for(int canvas_cycle=0; canvas_cycle<height*2; canvas_cycle++){
  //current bitmap to display
  charBuffer=canvas_cycle/(height/2);
  for(int row=0; row<height; row++){
    for(int col=0; col<width; col++){
      color=0;
      if( //Draw bitmaps based on values in above arrays
        draw(col, row, locations[charBuffer][0], locations[charBuffer][1],locations[charBuffer][2], locations[charBuffer][3], countBuffer[charBuffer]) 
      ){
        color=H2R_HSBtoRGB(row*4+(360-cycle),255,brightness);
      }
      leds.setPixel(mapPixel((row*width)+col),color);
    }
  }
  leds.show();
  cycle++;
  cycle=cycle%cycle_mod;
  }
}

void victory(){
  for(int row=0; row<height; row++){
    for(int col=0; col<width; col++){
      color=0;
      if(row>48 && row<110){
       // if(col>4 && col<10){
          leds.setPixel(mapPixel((row*width)+col),color);
        //}
      }
      if(draw(col, row, 4,53,7,53, winnerbmp)){
        color=H2R_HSBtoRGB(row*4+(360-(abs(cycle*5)%360)),255,brightness+10);
      leds.setPixel(mapPixel((row*width)+col),color);
      }
    }
  }
}

void updateClock(){
  long now = millis()-raceBegin;
  int min = now/60000;
  float sec = (now%60000)/1000.0;
  Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0><CJ><CS 1><T>");
  Serial1.print(min);
  Serial1.print(":");
  Serial1.printf("%05.2f", sec);
  Serial1.print("</T>\r");
}

void soloClock(unsigned long victorTime){
  unsigned long now = victorTime-raceBegin;
  unsigned int min = now/60000;
  float sec = (now%60000)/1000.0;
  Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0><CJ><CS 1><T>");
  Serial1.print(min);
  Serial1.print(":");
  Serial1.printf("%05.2f", sec);
  Serial1.print("</T>\r");
}

void victoryClock(long victorTime){
  long now = victorTime-raceBegin;
  int min = now/60000;
  float sec = (now%60000)/1000.0;
  if(millis()%800>400){
    Serial1.print("<ID 0><CLR>\r");
  }
  else{
    Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0><CJ><CS 1><T>");
    Serial1.print(min);
    Serial1.print(":");
    Serial1.printf("%05.2f", sec);
    Serial1.print("</T>\r");
  }
}

void finaleClock(){
  long now;
  bool displayFlip=digitalRead(dip1);
  bool score=0;
  char * justify = "<CJ>";
  if(displayFlip != (millis()%3000<1500)){  now = myTime-raceBegin; score=0; }
  else{ now = opponentTime-raceBegin; score=1; }
  if(displayFlip != score){ justify="<LJ>"; }
  else{ justify="<RJ>"; }
  int min = now/60000;
  float sec = (now%60000)/1000.0;
  Serial1.print("<ID 0><CLR><WIN 0 0 287 31><POS 0 0>");
  Serial1.print(justify);
  Serial1.print("<CS 1><T>");
  Serial1.print(min);
  Serial1.print(":");
  Serial1.printf("%05.2f", sec);
  Serial1.print("</T>\r");
}

void idleDisplay(){
  for(int row=0; row<height; row++){
    for(byte col=0; col<width; col++){      
        color=H2R_HSBtoRGB(((row)+(col)+cycle)%359,255,brightness);
        leds.setPixel(mapPixel((row*width)+col),color);
    }
  }
  cycle+=5;
  cycle=cycle%cycle_mod;
}

void graphDisplay(float graphPercent){
  for(int row=0; row<height; row++){
    for(byte col=0; col<width; col++){      
        color=H2R_HSBtoRGB(100,255,brightness);
        if(row<height-(graphPercent*height)){
          color=0;
        }
        leds.setPixel(mapPixel((row*width)+col),color);
    }
  }
  cycle+=5;
  cycle=cycle%cycle_mod;
}

void checkeredFlag(){
  for(int row=0; row<height; row++){
    for(byte col=0; col<width; col++){
      if((row+(cycle/2))%8<4){ 
        if((col+(cycle/4))%8<4){bright=brightness;}
        else{bright=0;} 
      }
      else{ 
        if((col+(cycle/4))%8<4){bright=0;}
        else{bright=brightness;} 
      }
        color=H2R_HSBtoRGB(20,20,bright);
        leds.setPixel(mapPixel((row*width)+col),color);
    }
  }
  cycle+=1;
  cycle=cycle%cycle_mod;
}

//Clear graph array
void clearGraph(){
  for(int i=0; i<height; i++){
    graph[i]=0;
  }
}

int mapPixel(int pxl){
//Variables for mapping function
  int return_pxl;
  int x=pxl%width;
  int y=pxl/width;
  if(y%2){
    return_pxl=pxl;
  }
  else{
    return_pxl=((y+1)*width)-1-x;
  }
  return return_pxl;
}

void updateOpponent(){
  if(micros()-lastRx > 40){  send485(myState);  }
  if(Serial2.available()){
    while(Serial2.available()){
      byte status=Serial2.read();
      lastRx=micros();
      switch (status){
        case IDLE:
          //Opponent finished
          opponentState=IDLE;
          break;
        case ACTIVE:
          //Opponent idle
          opponentState=ACTIVE;
          break;
        case FORFEIT:
          //Opponent quit
          opponentState=FORFEIT;
          break;
        case FINISH:
          //Opponent quit
          opponentState=FINISH;
          break;
        case 0:
          break;
        default:
          Serial.println(status);
          break;
      }
    }
    if((prevOpponentState!=FINISH && opponentState==FINISH)||(prevOpponentState!=FORFEIT && opponentState==FORFEIT)){
      prevOpponentState=opponentState;
      opponentTime=millis();
    }
  }
  comsFailure = (micros()-lastRx > comsTimeout) ? 1:0 ;
}

void send485(byte toSend){
  digitalWrite(RE2_485,1);
  digitalWrite(DE2_485,1);
  Serial2.write(toSend);
  Serial2.flush();
  digitalWrite(DE2_485,0);//Clear drive enable
  digitalWrite(RE2_485,0);//Set active low receive enable
}

void heartbeatISR(){
  hb=!hb;
  digitalWrite(led,hb);
} 

void updateFinishline(){
  float scaleFactor=analogRead(pot1)/1024.0;
  finishline=(scaleFactor*(finishlineMax-finishlineMin))+finishlineMin;
}
